
function muestraCuadratica() {
    let input = document.getElementsByName('datos');
    let divEcuacion = document.getElementById('eq-c');
    divEcuacion.innerHTML = 'x =<div class="fraction"><span class="fup"><i>-</i>(' + input[1].value + ')±<!--Raiz--><span class="radical">&radic;</span><span class="radicand"><i>(' + input[1].value + ')</i><sup>2</sup> -4(' + input[0].value + ')(' + input[2].value + ')</span><!--Raiz--></span><span class="bar">/</span><span class="fdn"><i>2</i>(' + input[0].value + ')</span></div>';
    mostrarResultados();
}

function mostrarResultados() {
    let input = document.getElementsByName('datos');
    let divEcuacion = document.getElementById('eq-c');
    let a = input[0].value;
    let b = input[1].value;
    let c = input[2].value;
    let disc = b * b - 4 * a * c;
    if(disc<0){
    divEcuacion.innerHTML += '<br><h5>ERROR-> LA RAIZ ES IMAGINARIA, EL RESULTADO DENTRO DE LA RAIZ ES NEGATIVO</h5>' ;
    
    }else{


    let x2 = (-b - Math.sqrt(disc)) / (2 * a);
    let x1 = (-b + Math.sqrt(disc)) / (2 * a); 
    divEcuacion.innerHTML += '<br><div class="xx1"><b><span class="sy-b">X</span><sub>1</sub>= ' + x1.toFixed(2) + ' </b></div><div class="xx2"><b><span class=" sy-b">X</span><sub>2</sub>= ' + x2.toFixed(2)+"</b></div>";
}

}


function muestraFuncionFormalizada() {
    let x="";
    let input = document.getElementsByName('datos');
    let aa = agregarSignoAPositivos(input[0], 0);
    if(aa=="Error"){    return calculo.innerHTML = "<b><span style='color:red'>" + aa +" el valor 'a' o el denominador no puede ser 0 Cero</b>" ;
    }else{
    let bb = agregarSignoAPositivos(input[1], 1);
    if(bb==="XCero"){bb=""; }else{x="x";} 
                     

    let cc = agregarSignoAPositivos(input[2], 2);
    let calculo = document.getElementById("calculo");
    return calculo.innerHTML = "<b>Cálculo para: <span style='color:green'>" + aa + " x²</span> <span style='color:red'>" + bb + x +"</span><span style='color:blue'>" + cc + "</span>=0</b>";
}}


function agregarSignoAPositivos(input, i) {
    if (i == 0) {
        if (input.value == 0) { return "Error"; 
        } else return input.value;}

    if (i == 1) {
        if (input.value == 0 || input.value == null) { return "XCero"; 
        } }
        
    if (input.value > 0.0) return "+" + input.value;
    return input.value;

}

function calcularCuadratica() {
    let error = document.getElementById("error");
    let datos = document.getElementsByName("datos");
    muestraFuncionFormalizada();
    muestraCuadratica();
}

function obtenerValores() {
    let input=document.getElementsByName('datos');
    let array=[];
    a = parseFloat(input[0].value);
    b = parseFloat(input[1].value);
    c = parseFloat(input[2].value);

    let desde= parseInt(document.getElementById("inputdesde").value);
    let hasta= parseInt(document.getElementById("inputhasta").value);
    let nr= (hasta-desde);
    
    for (let index = 0; index <= nr; index++) {
        let op = (a * Math.pow(index, 2)) + (b * index) + c;

        array.push([((a * Math.pow(index, 2)) + (b * index) + c)]);        
    }
    
    return array;
}



function vertabla() {
    let arr= obtenerValores();
    console.log(obtenerValores());
  var data = new google.visualization.DataTable(); 
        data.addColumn('number', 'X');
        data.addColumn('number', 'Y');
        
        // data.addRows( i, arr[i] );
        let desde= parseInt(document.getElementById("inputdesde").value);
        let hasta= parseInt(document.getElementById("inputhasta").value);
        let nrows= (hasta-desde);
        //mas +1 por el 0 que ocupa una fila
        data.addRows(nrows+1);
        
        //ciclo que cree las veces de la fila de la columna 1
         desde=desde-1;
         for(i=1; i<=nrows+1; i++){
         data.setCell(i-1, 0, desde+=1);
      }

        //columna 2
        var inicia= -1;
        for(i=1; i<=nrows+1;i++){
        
           data.setCell(i-1, 1,parseFloat(obtenerValores()[inicia+=1]));
        }
   
        var table = new google.visualization.Table(document.getElementById('eq-c'));


     table.draw(data, {width: '10%', height: '100%'});


}



 function vergrafica() {
    
        let arr = obtenerValores();
      var data = new google.visualization.DataTable();
      data.addColumn('number', 'X');
      data.addColumn('number', 'Y');
 //prueba     
 // data.addRows( i, arr[i] );
        let desde= parseInt(document.getElementById("inputdesde").value);
        let hasta= parseInt(document.getElementById("inputhasta").value);
        let nrows= (hasta-desde);
        //mas +1 por el 0 que ocupa una fila
        data.addRows(nrows+1);
        
        //ciclo que cree las veces de la fila de la columna 1
         desde=desde-1;
         for(i=1; i<=nrows+1; i++){
         data.setCell(i-1, 0, desde+=1);
      }

     
        //columna 2
        var inicia= -1;
        for(i=1; i<=nrows+1;i++){
        
           data.setCell(i-1, 1,parseFloat(obtenerValores()[inicia+=1]));
        }
    
        let input = document.getElementsByName('datos');
        let aa = agregarSignoAPositivos(input[0], 0);
        let bb = agregarSignoAPositivos(input[1], 1);
        let cc = agregarSignoAPositivos(input[2], 2);
        //prueba
      var options = {
        chart: {
          title:" ",
          subtitle: "Grafico para la fúncion: y=" + aa + " x²" + bb + "x" + cc + "=0"
        },
        width: 700,
        height: 500 };

      var chart = new google.charts.Line(document.getElementById('eq-c'));

      chart.draw(data, google.charts.Line.convertOptions(options));
    }